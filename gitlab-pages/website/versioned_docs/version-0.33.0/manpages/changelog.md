print the ligo changelog

ligo changelog

Dump the LIGO changelog to stdout.

=== flags ===

\[\--display-format format\] the format that will be used by the CLI.
Available formats are \'dev\', \'json\', and \'human-readable\'
(default). When human-readable lacks details (we are still tweaking it),
please contact us and use another format in the meanwhile. (alias:
\--format) \[-help\] print this help text and exit (alias: -?)
